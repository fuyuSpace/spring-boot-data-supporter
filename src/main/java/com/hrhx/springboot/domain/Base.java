package com.hrhx.springboot.domain;

public class Base {
	
	public static final String TRUE="true";
	public static final String FALSE="false";
	
	/**
	 * 全拼
	 */
	public static final String F="f";
	/**
	 * 首拼
	 */
	public static final String S="s";
	
}
